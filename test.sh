#!/bin/bash

arch=`dpkg --print-architecture`
echo "Architecture is $arch"

hostname=`awk '{print $1}' /etc/hostname`
cpus=$(nproc)

#OpenMP env vars
export OMP_BLOCKTIME=0
export OMP_PLACES=cores
export OMP_PROC_BIND=close
#export KMP_AFFINITY=verbose,granularity=fine,spread
#export OMP_NUM_THREADS=96

runtime=omp
test_type=(dpotrf) #(dgbtrf) #(dpotrf) #(fib fft sort health nqueens)
test_args=(12000) #(10000 12000 14000 16000) # 18000 20000)
num_threads=(1 2 4 8 12 24 48 96 192 384) #(96) #(24 48 96 192)
num_blocks=(256) #(160 256 352 448 544 640 736 848 976 1024) #(128 160 192)
#num_blocks=(192 256 320 384 448 512 576 640 768 912 976 1024)
for type in "${test_type[@]}"
do
  for args in "${test_args[@]}"
  do
    summaryfilename=${hostname}-${runtime}-${type}-scaling-$args
    for t in "${num_threads[@]}"
    do    
      #summaryfilename=${hostname}-${runtime}-${type}-blocks-$t-$args
      for b in "${num_blocks[@]}"
      do
        for run in {1..3}
        do
          export OMP_NUM_THREADS=$t
          echo OMP_NUM_THREADS=$t 2>&1 | tee -a $summaryfilename
          ./build/plasmatest $type --dim=$args --nb=$b 2>&1 | tee -a $summaryfilename
	        echo ""
        done
      done
    done
  done
done
