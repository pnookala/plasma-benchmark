#!/bin/bash

cmake .. -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DOpenMP_C_INCLUDE_DIR=/usr/local/include -DOpenMP_C_LIBRARY=/usr/local/lib
